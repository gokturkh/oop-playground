<?php

namespace HG;

use HG\Car\Car;
use HG\Engines\DieselEngine;

/**
 * Class DieselCar
 *
 * @package HG
 */
final class DieselCar extends Car
{
    /**
     * DieselCar constructor.
     * A diesel car may only have a diesel engine.
     *
     * @param  string        $brand
     * @param  DieselEngine  $engine
     */
    public function __construct(string $brand, DieselEngine $engine)
    {
        parent::__construct($brand, $engine);
    }

    /**
     * {@inheritdoc}
     */
    public function startEngine(): void
    {
        $this->engine->startEngine();
    }

    /**
     * {@inheritdoc}
     */
    public function stopEngine(): void
    {
        $this->engine->stopEngine();
    }

    /**
     * {@inheritdoc}
     */
    public function moveCar(): void
    {
        echo 'Car is moving!' . PHP_EOL;
    }

    /**
     * {@inheritdoc}
     */
    public function stopCar(): void
    {
        echo 'Car is stopping!' . PHP_EOL;
    }
}
