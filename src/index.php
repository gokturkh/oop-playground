<?php

/*
 * require dirname(__DIR__, 1).'/vendor/autoload.php';
 * > php 7
 * Same as below
 */

require __DIR__ . '/../vendor/autoload.php';

use HG\BenzineCar;
use HG\Log\DatabaseLogger;
use HG\DieselCar;
use HG\Engines\BenzineEngine;
use HG\Engines\DieselEngine;
use HG\Log\FileLogger;
use HG\Log\Logs;

$benzineEngine = new BenzineEngine(4000, 7000, 10000);
$dieselEngine = new DieselEngine(2000, 4000, 6500);

$audi = new BenzineCar('Audi', $benzineEngine);
print('Car Brand: ' . $audi->getBrand() . PHP_EOL);
print('Engine Type: ' . $audi->getEngineType() . PHP_EOL);
print('Engine Volume: ' . $audi->getEngineVolume() . PHP_EOL);
print('Engine Torque: ' . $audi->getEngineTorque() . PHP_EOL);
print('Engine RPM: ' . $audi->getEngineRpm() . PHP_EOL);

$audi->startEngine();
$audi->moveCar();
$audi->stopCar();
$audi->stopEngine();

print(PHP_EOL);
print(PHP_EOL);

$vw = new DieselCar('Volkswagen', $dieselEngine);
print('Car Brand: ' . $vw->getBrand() . PHP_EOL);
print('Engine Type: ' . $vw->getEngineType() . PHP_EOL);
print('Engine Volume: ' . $vw->getEngineVolume() . PHP_EOL);
print('Engine Torque: ' . $vw->getEngineTorque() . PHP_EOL);
print('Engine RPM: ' . $vw->getEngineRpm() . PHP_EOL);

$vw->startEngine();
$vw->moveCar();
$vw->stopCar();
$vw->stopEngine();



/* $fileLogger = new FileLogger();
$logger = new Log($fileLogger);
$logger->log();

echo PHP_EOL;

$databaseLogger = new DatabaseLogger();
$logger = new Log($databaseLogger);
$logger->log(); */
