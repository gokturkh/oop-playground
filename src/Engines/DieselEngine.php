<?php

namespace HG\Engines;

use HG\Engine\Engine;
use HG\Engine\EngineTypes;

/**
 * Class DieselEngine
 *
 * @package HG\Engines
 */
final class DieselEngine extends Engine
{
    /**
     * DieselEngine constructor.
     *
     * @param  int  $volume  Volume of the engine.
     * @param  int  $torque  Torque of the engine.
     * @param  int  $rpm     RPM of the engine.
     */
    public function __construct(int $volume, int $torque, int $rpm)
    {
        parent::__construct(EngineTypes::ENGINE_TYPE_DIESEL, $volume, $torque, $rpm);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return parent::getType();
    }

    /**
     * @return int
     */
    public function getVolume(): int
    {
        return parent::getVolume();
    }

    /**
     * @return int
     */
    public function getTorque(): int
    {
        return parent::getTorque();
    }

    /**
     * @return int
     */
    public function getRpm(): int
    {
        return parent::getRpm();
    }

    /**
     * @inheritDoc
     */
    public function startEngine(): void
    {
        echo 'Engine started.' . PHP_EOL;
    }

    /**
     * @inheritDoc
     */
    public function stopEngine(): void
    {
        echo 'Engine stopped.' . PHP_EOL;
    }
}
