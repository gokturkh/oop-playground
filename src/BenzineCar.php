<?php

namespace HG;

use HG\Car\Car;
use HG\Engines\BenzineEngine;

/**
 * Class BenzineCar
 *
 * @package HG
 */
final class BenzineCar extends Car
{
    /**
     * BenzineCar constructor.
     * A benzine car may only have a benzine engine.
     *
     * @param  string         $brand
     * @param  BenzineEngine  $engine
     */
    public function __construct(string $brand, BenzineEngine $engine)
    {
        parent::__construct($brand, $engine);
    }

    /**
     * {@inheritdoc}
     */
    public function startEngine(): void
    {
        $this->engine->startEngine();
    }

    /**
     * {@inheritdoc}
     */
    public function stopEngine(): void
    {
        $this->engine->stopEngine();
    }

    /**
     * {@inheritdoc}
     */
    public function moveCar(): void
    {
        echo 'Car is moving!' . PHP_EOL;
    }

    /**
     * {@inheritdoc}
     */
    public function stopCar(): void
    {
        echo 'Car is stopping!' . PHP_EOL;
    }
}
