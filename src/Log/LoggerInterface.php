<?php

namespace HG\Log;

/**
 * Interface LoggerInterface
 *
 * @package HG
 */
interface LoggerInterface
{
    /**
     * Log.
     *
     * @return void
     */
    public function log();
}
