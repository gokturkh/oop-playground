<?php

namespace HG\Log;

/**
 * Class Log
 *
 * @package HG
 */
class Logs
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;

    /**
     * Log constructor.
     *
     * @param  LoggerInterface  $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     *{@inheritDoc}
     */
    public function log()
    {
        $this->logger->log();
    }
}
