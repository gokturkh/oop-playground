<?php

namespace HG\Log;

/**
 * Class FileLogger
 *
 * @package HG
 */
class FileLogger implements LoggerInterface
{

    /**
     *{@inheritDoc}
     */
    public function log()
    {
        echo 'Logged into file.';
    }
}
