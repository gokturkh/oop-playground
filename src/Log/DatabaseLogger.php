<?php

namespace HG\Log;

/**
 * Class DatabaseLogger
 *
 * @package HG
 */
class DatabaseLogger implements LoggerInterface
{

    /**
     *{@inheritDoc}
     */
    public function log()
    {
        echo 'Logged into database.';
    }
}
