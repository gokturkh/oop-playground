<?php

namespace HG\Engine;

/**
 * Class EngineTypes
 *
 * @package HG\Engine
 */
final class EngineTypes
{
    /**
     *
     */
    public const ENGINE_TYPE_BENZINE = 'BENZINE';
    /**
     *
     */
    public const ENGINE_TYPE_DIESEL = 'DIESEL';
    /**
     *
     */
    public const ENGINE_TYPE_ELECTRIC = 'ELECTRIC';
    /**
     *
     */
    public const ENGINE_TYPE_HYBRID = 'HYBRID';
}
