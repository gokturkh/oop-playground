<?php

namespace HG\Engine;

/**
 * Class Engine
 *
 * @package HG\Engine
 */
abstract class Engine
{
    /**
     * Type of engine. e.g. BENZINE, DIESEL, ELECTRIC, HYBRID
     *
     * @var string
     */
    private string $type;
    /**
     * Volume of engine.
     *
     * @var int
     */
    private int $volume;
    /**
     * Torque of engine.
     *
     * @var int
     */
    private int $torque;
    /**
     * RPM of engine.
     *
     * @var int
     */
    private int $rpm;

    /**
     * Engine constructor.
     *
     * @param  string  $type
     * @param  int     $volume
     * @param  int     $torque
     * @param  int     $rpm
     */
    public function __construct(string $type, int $volume, int $torque, int $rpm)
    {
        $this->type = $type;
        $this->volume = $volume;
        $this->torque = $torque;
        $this->rpm = $rpm;
    }

    /**
     * Returns engine type.
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Returns engine volume.
     *
     * @return int
     */
    public function getVolume(): int
    {
        return $this->volume;
    }

    /**
     * Returns engine torque.
     *
     * @return int
     */
    public function getTorque(): int
    {
        return $this->torque;
    }

    /**
     * Returns engine RPM.
     *
     * @return int
     */
    public function getRpm(): int
    {
        return $this->rpm;
    }

    /**
     * Starts engine.
     *
     * @return void
     */
    abstract public function startEngine(): void;

    /**
     * Stops engine.
     *
     * @return void
     */
    abstract public function stopEngine(): void;
}
