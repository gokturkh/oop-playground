<?php

namespace HG\Car;

use HG\Engine\Engine;

/**
 * Class Car
 *
 * @package HG\Car
 */
abstract class Car
{
    /**
     * Name of the car brand.
     *
     * @var string
     */
    private string $brand;
    /**
     * Engine type of Engine class.
     *
     * @var Engine
     */
    protected Engine $engine;

    /**
     * Car constructor.
     *
     * @param  string  $brand
     * @param  Engine  $engine
     */
    public function __construct(string $brand, Engine $engine)
    {
        $this->brand = $brand;
        $this->engine = $engine;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @return string
     */
    public function getEngineType(): string
    {
        return $this->engine->getType();
    }

    /**
     * @return int
     */
    public function getEngineVolume(): int
    {
        return $this->engine->getVolume();
    }

    /**
     * @return int
     */
    public function getEngineTorque(): int
    {
        return $this->engine->getTorque();
    }

    /**
     * @return int
     */
    public function getEngineRpm(): int
    {
        return $this->engine->getRpm();
    }

    /**
     * Starts engine.
     */
    abstract public function startEngine();

    /**
     * Stops engine.
     */
    abstract public function stopEngine();

    /**
     * Moves car.
     */
    abstract public function moveCar();

    /**
     * Stops car.
     */
    abstract public function stopCar();
}
